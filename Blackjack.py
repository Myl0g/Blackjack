# Milo J. Gilad, 9th Grade Secretary/Treasurer & Robotics Project-Manager Elect
# 1/25/16
# Blackjack Simulator 2017 (Copyright Milo J. Gilad, 9th Grade Secretary/Treasurer)

# Importing Graphics for GUI
from graphics import *
# Importing time for sleep command
from time import sleep
# Importing Random for Shuffling
import random

def bank(win, bet, bank):
    # Setting bank and bet values
    bankBal = bank
    betVal = bet
    if bank <= 0:
        notEnough = Text(Point(400, 300), "You've Gone Bankrupt! The game will now exit.")
        notEnough.setSize(20)
        notEnough.draw(win)
        sleep(5)
        exit()
    else:
        # Drawing Bank Value Text
        bankText = Text(Point(100, 70), "Bank Balance: " + str(bankBal))
        bankText.setSize(15)
        bankText.draw(win)
        # Drawing Bet Value Text
        betText = Text(Point(100, 90), "Current Bet: " + str(betVal))
        betText.setSize(15)
        betText.draw(win)
    return bankBal, betVal, bankText, betText

def addCards(win, cardList):
    alphabetList = ['c', 'd', 'h', 's']
    altAlphaList = ['a', 'j', 'k', 'q', 't']
    cardPatNum = 2
    alphaCount = 0
    altAlphaCount = 0
    # Adding cards 2c through 9s
    for i in range(8):
        for j in range(4):
            card = 'cards/' + str(cardPatNum) + alphabetList[alphaCount] + '.gif'
            alphaCount+=1
            cardList.append(card)
        alphaCount = 0
        cardPatNum+=1
    # Adding cards ac through ts (except b and j)
    for i in range (5):
        for i in range(4):
            card = 'cards/' + altAlphaList[altAlphaCount] + alphabetList[alphaCount] + '.gif'
            alphaCount+=1
            cardList.append(card)
        alphaCount = 0
        altAlphaCount+=1
    return cardList
def drawCards(win, shuffledDeck, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, dealerscore, GraphicsCards, cardBack, player, dealer):
    locCalcPlayer = 30*playerCardLoc
    locCalcDealer = 30*dealerCardLoc
    if player == True:
        playerCard = Image(Point(179 + locCalcPlayer, 479), shuffledDeck[0])
        playerCard.draw(win)
        GraphicsCards.append(playerCard)
        playerCardsOnScreen.append(shuffledDeck[0])
        shuffledDeck.pop(0)
        playerCardCount+=1
        playerCardLoc+=1
    if dealer == True and dealerscore < 17:
        if dealerCardCount is 0:
            dealerCard = Image(Point(179 + locCalcDealer, 100), shuffledDeck[0])
            dealerCard.draw(win)
            GraphicsCards.append(dealerCard)
            dealerCardsOnScreen.append(shuffledDeck[0])
            shuffledDeck.pop(0)
            dealerCardCount+=1
        elif dealerCardCount is 1:
            dealerCard = Image(Point(179 + locCalcDealer, 100), shuffledDeck[0])
            dealerCard.draw(win)
            GraphicsCards.append(dealerCard)
            dealerCardsOnScreen.append(shuffledDeck[0])
            shuffledDeck.pop(0)
            cardBack = Image(Point(179 + locCalcDealer, 100), 'cards/b.gif')
            cardBack.draw(win)
            GraphicsCards.append(cardBack)
            dealerCardCount+=1
        elif dealerCardCount > 1:
            dealerCard = Image(Point(179 + locCalcDealer, 100), shuffledDeck[0])
            dealerCard.draw(win)
            GraphicsCards.append(dealerCard)
            dealerCardsOnScreen.append(shuffledDeck[0])
            shuffledDeck.pop(0)
            dealerCardCount+=1
        dealerCardLoc+=1
    return GraphicsCards, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, cardBack
def shuffle(win, cardList):
    # Creating shuffledDeck list for storing shuffled card values
    shuffledDeck = cardList
    random.shuffle(shuffledDeck)
    return shuffledDeck
def tallyScore(score, dealerscore, playerCardsOnScreen, dealerCardsOnScreen, playerAceCount, dealerAceCount, player, dealer):
    print("score will now be determined based on the cards on screen.")
    if player == True:
        for i in range(len(playerCardsOnScreen)):
            if playerCardsOnScreen[i][6] == '2':
                print(playerCardsOnScreen[i] + " worthy of 2 points.")
                score+=2
            elif playerCardsOnScreen[i][6] == '3':
                print(playerCardsOnScreen[i] + " worthy of 3 points.")
                score+=3
            elif playerCardsOnScreen[i][6] == '4':
                print(playerCardsOnScreen[i] + " worthy of 4 points.")
                score+=4
            elif playerCardsOnScreen[i][6] == '5':
                print(playerCardsOnScreen[i] + " worthy of 5 points.")
                score+=5
            elif playerCardsOnScreen[i][6] == '6':
                print(playerCardsOnScreen[i] + " worthy of 6 points.")
                score+=6
            elif playerCardsOnScreen[i][6] == '7':
                print(playerCardsOnScreen[i] + " worthy of 7 points.")
                score+=7
            elif playerCardsOnScreen[i][6] == '8':
                print(playerCardsOnScreen[i] + " worthy of 8 points.")
                score+=8
            elif playerCardsOnScreen[i][6] == '9':
                print(playerCardsOnScreen[i] + " worthy of 9 points.")
                score+=9
            elif playerCardsOnScreen[i][6] == 'j' or playerCardsOnScreen[i][6] == 'k' or playerCardsOnScreen[i][6] == 'q' or playerCardsOnScreen[i][6] == 't':
                print(playerCardsOnScreen[i] + " worthy of 10 points.")
                score+=10
        for i in range(len(playerCardsOnScreen)):
            if playerCardsOnScreen[i][6] == 'a':
                print("Ace Detected.")
                aceOrNoAce = score + 11
                if aceOrNoAce > 21:
                    print("strategically, ace is worth 1.")
                    score+=1
                elif aceOrNoAce <= 21:
                    print("strategically, ace is worth 11.")
                    score+=11
                    playerAceCount+=1
    if dealer == True:
        for i in range(len(dealerCardsOnScreen)):
            if dealerCardsOnScreen[i][6] == '2':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 2 points.")
                dealerscore+=2
            elif dealerCardsOnScreen[i][6] == '3':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 3 points.")
                dealerscore+=3
            elif dealerCardsOnScreen[i][6] == '4':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 4 points.")
                dealerscore+=4
            elif dealerCardsOnScreen[i][6] == '5':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 5 points.")
                dealerscore+=5
            elif dealerCardsOnScreen[i][6] == '6':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 6 points.")
                dealerscore+=6
            elif dealerCardsOnScreen[i][6] == '7':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 7 points.")
                dealerscore+=7
            elif dealerCardsOnScreen[i][6] == '8':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 8 points.")
                dealerscore+=8
            elif dealerCardsOnScreen[i][6] == '9':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 9 points.")
                dealerscore+=9
            elif dealerCardsOnScreen[i][6] == 'j' or dealerCardsOnScreen[i][6] == 'k' or dealerCardsOnScreen[i][6] == 'q' or dealerCardsOnScreen[i][6] == 't':
                print("dealer card " + dealerCardsOnScreen[i] + " worthy of 10 points.")
                dealerscore+=10
        for i in range(len(dealerCardsOnScreen)):
            if dealerCardsOnScreen[i][6] == 'a':
                print("Ace Detected.")
                aceOrNoAce = dealerscore + 11
                if aceOrNoAce > 21:
                    print("strategically, ace is worth 1.")
                    dealerscore+=1
                elif aceOrNoAce <= 21:
                    print("strategically, ace is worth 11.")
                    dealerscore+=11
                    dealerAceCount+=1
    while score > 21 and playerAceCount > 0:
        score = score - 10
    while dealerscore > 21 and dealerAceCount >= 1:
        dealerscore = dealerscore - 10
    winner = "notclear"
    return winner, score, dealerscore, playerAceCount, dealerAceCount
def whoWon(winner, score, dealerscore):
    if score <= 21 and dealerscore > 21:
        winner = "Yes"
    elif dealerscore <= 21 and score > 21:
        winner = "No"
    elif score < 21 and dealerscore < 21:
        if score > dealerscore:
            winner = "Yes"
        elif dealerscore > score:
            winner = "No"
        elif score == dealerscore:
            winner = "None"
    elif score > 21 and dealerscore > 21:
        if score > dealerscore:
            winner = "No"
        elif dealerscore > score:
            winner = "Yes"
        elif score == dealerscore:
            winner = "None"
    elif score == 21 and dealerscore == 21:
        winner = "None"
    elif score == 21 and dealerscore < 21:
        winner = "Yes"
    elif dealerscore == 21 and score < 21:
        winner = "No"
    return winner
def continueGame(bankBal, betVal, winner, win, endGame):
    if winner == "Yes":
        youWon = Text(Point(400, 300), "You Won! You bet has been doubled and deposited in your bank!")
        youWon.setSize(20)
        youWon.draw(win)
        sleep(5)
        youWon.undraw()
        bankBal = bankBal + betVal*2
        betVal = 0
        endGame = True
    elif winner == "No":
        youLose = Text(Point(400, 300), "You lose. ¯\_(ツ)_/¯")
        youLose.setSize(30)
        youLose.draw(win)
        sleep(3)
        youLose.undraw()
        betVal = 0
        endGame = True
    elif winner == "None":
        noWin = Text(Point(400, 300), "No one wins. Your bet has been refunded.")
        noWin.setSize(20)
        noWin.draw(win)
        sleep(3)
        noWin.undraw()
        bankBal = bankBal + betVal
        betVal = 0
        endGame = True
    return endGame, bankBal, betVal
def setupScreen(win):
    # Initial Setup
    print("drawing boxes and buttons...")
    DealerBox = Rectangle(Point(170, 50), Point(630, 150))
    DealerBox.setWidth(3)
    DealerBox.draw(win)
    PlayerBox = Rectangle(Point(170, 430), Point(630, 530))
    PlayerBox.setWidth(3)
    PlayerBox.draw(win)
    # Drawing Hit Text
    hitText = Text(Point(290, 580), "HIT")
    hitText.setSize(20)
    hitText.draw(win)
    hitTextBox = Rectangle(Point(271, 567), Point(308, 590))
    hitTextBox.draw(win)
    # Drawing Stay Text
    stayText = Text(Point(370, 580), "STAY")
    stayText.setSize(20)
    stayText.draw(win)
    stayTextBox = Rectangle(Point(340, 567), Point(400, 590))
    stayTextBox.draw(win)
    # Drawing Bet Text
    betText = Text(Point(450, 580), "BET")
    betText.setSize(20)
    betText.draw(win)
    betTextBox = Rectangle(Point(427, 567), Point(473, 590))
    betTextBox.draw(win)
    # Drawing Exit Key
    exitText = Text(Point(80, 200), "EXIT")
    exitText.setSize(20)
    exitText.draw(win)
    exitTextBox = Rectangle(Point(55, 185), Point(104, 213))
    exitTextBox.draw(win)
def main():
    print("Making window & setting options for window...")
    win = GraphWin("Blackjack Simulator 2017", 800, 600)
    endGame = False # Will determine when the game should be restarted.
    roundNotStarted = True # Will determine if betting is allowed.
    cardList = []
    cardBack = 0
    playerCardLoc = 1
    dealerCardLoc = 1 # To shorten program, cardLoc variable starts at one even though amount of cards is 0.
    playerCardsOnScreen = []
    dealerCardsOnScreen = []
    playerCardCount = 0
    dealerCardCount = 0
    playerAceCount = 0
    dealerAceCount = 0
    score = 0
    dealerscore = 0
    GraphicsCards = []
    setupScreen(win)
    bankBal, betVal, bankText, betText = bank(win, 0, 200)
    cardList = addCards(win, cardList)
    shuffledDeck = shuffle(win, cardList)
    GraphicsCards, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, cardBack = drawCards(win, shuffledDeck, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, dealerscore, GraphicsCards, cardBack, True, True)
    GraphicsCards, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, cardBack = drawCards(win, shuffledDeck, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, dealerscore, GraphicsCards, cardBack, True, True)
    winner, score, dealerscore, playerAceCount, dealerAceCount = tallyScore(score, dealerscore, playerCardsOnScreen, dealerCardsOnScreen, playerAceCount, dealerAceCount, True, True)
    endGame, bankBal, betVal = continueGame(bankBal, betVal, winner, win, endGame)
    playerCardsOnScreen = []
    dealerCardsOnScreen = []
    playerScoreDisplay = Text(Point(700, 430), "Player Score: " + str(score))
    playerScoreDisplay.setSize(17)
    playerScoreDisplay.draw(win)
    # At this point, 2 cards have been drawn. It's the player's turn now.
    while True:
        location = win.getMouse()
        while location.getX() <= 800 and location.getX() >= 0 and location.getY() >= 0 and location.getY() <= 600:
            if location.getX() <= 104 and location.getX() >= 55 and location.getY() >= 185 and location.getY() <= 213:
                exit()
            elif location.getX() <= 473 and location.getX() >= 427 and location.getY() >= 567 and location.getY() <= 590:
                if roundNotStarted == True:
                    print("Increasing Bet by 30...")
                    bankText.undraw()
                    betText.undraw()
                    bankBal, betVal, bankText, betText = bank(win, betVal + 30, bankBal - 30)
                else:
                    notAble = Text(Point(400, 300), "You must bet before the round starts, not after!")
                    notAble.setSize(30)
                    notAble.draw(win)
                    sleep(3)
                    notAble.undraw()
            elif location.getX() <= 400 and location.getX() >= 340 and location.getY() >= 567 and location.getY() <= 590:
                roundNotStarted = False
                while dealerscore < 17:
                    GraphicsCards, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, cardBack = drawCards(win, shuffledDeck, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, dealerscore, GraphicsCards, cardBack, False, True)
                    winner, score, dealerscore, playerAceCount, dealerAceCount = tallyScore(score, dealerscore, playerCardsOnScreen, dealerCardsOnScreen, playerAceCount, dealerAceCount, False, True)
                playerScoreDisplay.undraw()
                playerScoreDisplay = Text(Point(700, 430), "Player Score: " + str(score))
                playerScoreDisplay.setSize(17)
                playerScoreDisplay.draw(win)
                winner = whoWon(winner, score, dealerscore)
                cardBack.undraw()
                endGame, bankBal, betVal = continueGame(bankBal, betVal, winner, win, endGame)
                bankText.undraw()
                betText.undraw()
                bankBal, betVal, bankText, betText = bank(win, betVal, bankBal)
                playerCardsOnScreen = []
                dealerCardsOnScreen = []
                if endGame == True:
                    print("Restarting...")
                    for i in GraphicsCards:
                        i.undraw()
                    score = 0
                    dealerscore = 0
                    shuffledDeck = shuffle(win, cardList)
                    playerCardLoc = 1
                    dealerCardLoc = 1
                    dealerCardCount = 0
                    GraphicsCards, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, cardBack = drawCards(win, shuffledDeck, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, dealerscore, GraphicsCards, cardBack, True, True)
                    GraphicsCards, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, cardBack = drawCards(win, shuffledDeck, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, dealerscore, GraphicsCards, cardBack, True, True)
                    winner, score, dealerscore, playerAceCount, dealerAceCount = tallyScore(score, dealerscore, playerCardsOnScreen, dealerCardsOnScreen, playerAceCount, dealerAceCount, True, True)
                    endGame, bankBal, betVal = continueGame(bankBal, betVal, winner, win, endGame)
                    playerCardsOnScreen = []
                    dealerCardsOnScreen = []
                    cardList = []
                    shuffledDeck = []
                    cardList = addCards(win, cardList)
                    shuffledDeck = shuffle(win, cardList)
                    playerScoreDisplay.undraw()
                    playerScoreDisplay = Text(Point(700, 430), "Player Score: " + str(score))
                    playerScoreDisplay.setSize(17)
                    playerScoreDisplay.draw(win)
                    playerAceCount = 0
                    dealerAceCount = 0
                    roundNotStarted = True                    
                    endGame = False
            elif location.getX() <= 308 and location.getX() >= 271 and location.getY() >= 567 and location.getY() <= 590:
                roundNotStarted = False
                GraphicsCards, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, cardBack = drawCards(win, shuffledDeck, playerCardLoc, dealerCardLoc, playerCardCount, dealerCardCount, playerCardsOnScreen, dealerCardsOnScreen, dealerscore, GraphicsCards, cardBack, True, False)
                winner, score, dealerscore, playerAceCount, dealerAceCount = tallyScore(score, dealerscore, playerCardsOnScreen, dealerCardsOnScreen, playerAceCount, dealerAceCount, True, False)
                playerScoreDisplay.undraw()
                playerScoreDisplay = Text(Point(700, 430), "Player Score: " + str(score))
                playerScoreDisplay.setSize(17)
                playerScoreDisplay.draw(win)
                endGame, bankBal, betVal = continueGame(bankBal, betVal, winner, win, endGame)
                bankText.undraw()
                betText.undraw()
                bankBal, betVal, bankText, betText = bank(win, betVal, bankBal)
                playerCardsOnScreen = []
                dealerCardsOnScreen = []
            location = win.getMouse()
main()
